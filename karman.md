# Introduction

  This document holds the time sheets for my work during 2022 Summer Internship at VLEAD, IIIT Hyderabad.

# Work Logs

## Week 1 [2022-05-16 Mon] till [2022-05-20 Fri]

## Daily Logs

- Day1 [2022-05-16 Mon]: Introductory Meet assigned projects to work on , and discussed about the internship plan as we go through.
- Day2 [2022-05-17 Tue] : Given the documentation , and progress of the previous meet on the experiment , gone through the code base , understanding the code base.
- Day3 [2022-05-18 Wed] : Going through the codebase , understanding the code.
- Day4 [2022-05-19 Thu] : Going through the codebase , understanding the code , reviewing the changes to be done ( as given in suggestions and to/do section ) planning and choosing the experiment to work on.
- Day5 [2022-05-20 Fri] : Chosen experiment-10 to start with , started coding the experiment , analysed the changes , done basic coding for the experiment.

## Week 2 [2022-05-23 Mon] till [2022-05-27 Fri]

## Daily Logs

- Day1 [2022-05-23 Mon]: Done with the re-structuring the given code , made it modular and refined variable namings to convey more sense , There were 4 test cases , for which the given codebase had to be extended , planned for covering the same.
- Day2 [2022-05-24 Tue] : Test Case 1 & 2 done .
- Day3 [2022-05-25 Wed] : Test Case 3 & 4 done .
- Day4 [2022-05-26 Thu] : Bug Hunting , and display customisations ~ improved styling and display of the experiment components , fixed bugs .
- Day5 [2022-05-27 Fri] : Final touches , covering the experiment , adding descriptive error messages and fixing bugs , testing against edge cases ~ Code tested and uploaded on the git hub repository.

## Week 3 [2022-05-30 Mon] till [2022-06-03 Fri]

## Daily Logs
- Day1 [2022-05-30 Mon]: Added Error handling , form field highlighting for the experiment 10 , as suggested in the previous meet , made a pull request.
- Day2 [2022-05-31 Tue] : added and changed theory , procedure and instructions for the experiment ,  made suggested changed according to the code review - Code Refactoring.
- Day3 [2022-06-01 Wed] : reading and knowledge gathering about the implementation of the algorithm - Cohen Sutherland Algorithm  ( experiment 8 ). Went through the codebase , for experiment 8.
- Day4 [2022-06-02 Thu] : Started coding the experiment 8 from scratch , since the previous implementation wasn't coded in modular way , and had some bugs, that were difficult to fix by editing the codebase , since the implementation of the algorithm wasn't well planned , or in a proper flow .
- Day5 [2022-06-03 Fri] : Coding the experiment 8 , done ~20%.

## Week 4 [2022-06-06 Mon] till [2022-06-10 Fri]

## Daily Logs
- Day1 [2022-06-06 Mon] : Eperiment 8 , explored theory and implementation of the code , refactored it , handling of next function done.
- Day2 [2022-06-07 Tue] : Finished coding the experiment 8 . 
- Day3 [2022-06-08 Wed] : Fixed bugs , added error handling to the code .
- Day4 [2022-06-09 Thu] : Explored Lit.js , read the documentation , went through sample projects .
- Day5 [2022-06-10 Fri] :Built a sample Lit project to get started with it. Completed Experiment 8 , minor tweaking and display of error messages left.
  
## Week 5 [2022-06-13 Mon] till [2022-06-17 Fri]

## Daily Logs
- Day1 [2022-06-13 Mon] : Experiment 8 , improved the implementation of the code , fixed a few bugs.
- Day2 [2022-06-14 Tue] : Added display error messages function to the experiment. 
- Day3 [2022-06-08 Wed] : Modified the add/remove functions, fixed some bugs(display distortion on page reload).
- Day4 [2022-06-15 Thu] : Reading Lit.js documentation.
- Day5 [2022-06-16 Fri] : Trying out the Three.js functionality for components like ray casting for experiment 1,etc.


## Week 6 [2022-06-20 Mon] till [2022-06-24 Fri]

## Daily Logs
- Day1 [2022-06-20 Mon] : Bug Hunting in sample Lit component , exploring lit methods and documentation.
- Day2 [2022-06-21 Tue] : Read through the rating web component built in vanilla js , exploring the MDN documentation for the web components. 
- Day3 [2022-06-22 Wed] : Bug Hunting and final code review & merging of the experiment 8.
- Day4 [2022-06-23 Thu] : Started coding the basic structure of the Lit based rating web component.
- Day5 [2022-06-24 Fri] : Continuing coding the lit based web component.


## Week 7 [2022-06-27 Mon] till [2022-07-01 Fri]

## Daily Logs
- Day1 [2022-06-27 Mon] : Fixing the issues raised in the pull req of experiment 8.
- Day2 [2022-06-28 Tue] : Handled some leftover usecases, bugs , fixed and updated the repo . 
- Day3 [2022-06-29 Wed] : Looking through the codebase for experiment 1 and 3.
- Day4 [2022-06-30 Thu] : Reading and understanding Experiment 1 and coded modal component for the lit web component
- Day5 [2022-07-01 Fri] : Built the web component with basic working functionalities.

## Week 8 [2022-07-04 Mon] till [2022-07-08 Fri]

## Daily Logs
- Day1 [2022-07-04 Mon] : Work review meet , discussion on event based submission methods for the lit web component.
- Day2 [2022-07-05 Tue] : Added and merged the final changes pertaining to the experiment 8. 
- Day3 [2022-07-06 Wed] : Fixed structure and code for experiment , pushed to dev.
- Day4 [2022-07-07 Thu] : Merged the code to testing and checked the basic working , bug hunting.
- Day5 [2022-07-08 Fri] : Bug hunting and code review, for experiment 1 fixed the bugs.


## Week 9 [2022-07-11 Mon] till [2022-07-15 Fri]

## Daily Logs
- Day1 [2022-07-11 Mon] : Explored the events release and capture documentation on Lit and MDN web docs.
- Day2 [2022-07-12 Tue] : Adding events on the built web components. 
- Day3 [2022-07-13 Wed] : Separating the built web components into multiple sub-components which can be reused.
- Day4 [2022-07-14 Thu] : Fixed the issues raised in experiment 1 and merged to main .
- Day5 [2022-07-15 Fri] : Modal rating component separated.

## Week 10 [2022-07-18 Mon] till [2022-07-22 Fri]

## Daily Logs
- Day1 [2022-07-18 Mon] : Structuring the multiple components.
- Day2 [2022-07-19 Tue] : The rating display component separated and structured. 
- Day3 [2022-07-20 Wed] : Implemented and tested the sending and recieving of events based in Lit.js , pushed the code to the repo.
- Day4 [2022-07-21 Thu] : Changes were approved.
- Day5 [2022-07-22 Fri] : 

# Week Progress

| S.No  | Overall Experiment Status  |                        Previous Week Milestone                         |                                     Next Week Milestone                                     |                                                                                                             Issues                                                                                                             |
| :---: | :------------------------: | :--------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  1.   | Started with experiment 10 |             Reviewed and understood the existing codebase              |                                    Finish experiment 10                                     |                                         Will have to make major changes in the existing codebase , to account for all the possible test cases ( for slope of line to cover all cases )                                         |
|  2.   |  Experiment 10 completed   |                    Started coding the experiment 10                    |                                   Finishing experiment 8                                    |                                 Displaying error messages in experiment 10. According to suggestions have to make changes in the way the text is being rendered on the screen , in apt manner.                                 |
|  3.   |  Experiment 10 completed   |            Fixed the Experiment 10 and started experiment 8            |                                   Finishing experiment 8                                    | The previous team had a very poor implementation of the algorithm without any proper flow , making it difficult to make the code modular. In order to make the code modular , will have to code up the algorithm from scratch. |
|  4.   |   Experiment 8 completed   | Finished Experiment 8 , explored Lit.js for web-component track 2 task | Getting started with the web components and Lit.js , Getting familiar with the experiment 1 |                                                          Suggestions & improvements for the completed experiment 8 [ coloring scheme , rendering of the components ]                                                           |
|  5.   |   Experiment 8 completed ( detected some bugs ) , suggestions to be incorporated   | Finishing the Experiment 8 and incorporating the changes,   | Getting started with the web components and Lit.js , Getting familiar with the experiment 9 |                                                         As suggested , changed the font size issues ,and some text alignment issues in three js and the canvas element                                                         |
|  6.   |   Experiment 8 completed (fixing the bugs and incorporating the changes) built a sample lit based webcomponent  | Finished Experiment 8 , explored Lit.js for web-component track 2 task | Coding the Lit component , setting up the basic stricture of it. |                                                          Incorporate the final review changes for exp 8 and code up lit component alongside the modal components act weirdly on reset ,this issue to be fixed alongside some coloring suggestions ]                                                           |
|  7.   |   Incorporated the suggested changes in experiment 8 , basic coding of rating component done , Started with the experiment 1   | Built basic rating component | Start with the experiment 1 and submitted the basic component for review |                                                     Handling the svg images of the stars , instead of the unicode sequence , had some errors usingh the svgs , hence substituted it for unicode sequence                                                         |
|  8.   |  Basic Lit component done  | Start with the experiment 1 and submitted the basic component for review  | Coding complete the experiment 1  |                                                         Added the favicon images for stars , fixed the previous issue, need to look for handling of events in lit ( dispatching and catching the events ) and the colors of the shapes that get added to the canvas in experiment 1            |
|  9.   |  Finished coding up the experiment 1 , suggestions to be incorporated  | Coding complete the experiment 1 |As suggested in the review meet , separating the components to make it more re-usable and extensible |                                                          The wrapping around of the web-component to make it more extensible , and separating it into multiple components , faced some errors in getting the desired behaviour .                                                          |
|  10.   |   Finished coding the experiment 1 and reviewed the experiment 4 , handled events , etc. stuff in lit component , prototype was built and approved  |Intern Ends Here | |                                                       











